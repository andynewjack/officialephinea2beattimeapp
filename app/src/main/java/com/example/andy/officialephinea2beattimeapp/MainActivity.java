package com.example.andy.officialephinea2beattimeapp;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Handler handler = new Handler();
        final int delay = 1000; // milliseconds

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int myHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                int myMinute = Calendar.getInstance().get(Calendar.MINUTE);
                int mySecond = Calendar.getInstance().get(Calendar.SECOND);
                int myOffset = Calendar.getInstance().get(Calendar.ZONE_OFFSET) / (3600 * 1000);
//                int myOffset = (Calendar.getInstance().get(Calendar.ZONE_OFFSET) + Calendar.getInstance().get(Calendar.DST_OFFSET)) / (3600 * 1000);
//                boolean myDST = TimeZone.getDefault().inDaylightTime( new Date() );
//                if (myDST) {myOffset -= 1;}
                double ephineaBeats = (((((myHour - myOffset) * 3600) + (myMinute * 60) + (mySecond)) % 86400) / 86.4);
                int progress = (int) ((ephineaBeats - (int)ephineaBeats) * 100);

                // Capture the layout's TextView and set the string as its text
                TextView textView = findViewById(R.id.textView);
                textView.setText(String.valueOf((int)ephineaBeats));
                handler.postDelayed(this, delay);
                ProgressBar progressBar = findViewById(R.id.progressBar);
                progressBar.setProgress(progress);
            }
        }, delay);
    }
}
